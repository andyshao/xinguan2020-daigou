'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var crypto = _interopDefault(require('crypto'));

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var jwt_1 = createCommonjsModule(function (module) {
/*
 * jwt-simple
 *
 * JSON Web Token encode and decode module for node.js
 *
 * Copyright(c) 2011 Kazuhito Hokamura
 * MIT Licensed
 */

/**
 * module dependencies
 */



/**
 * support algorithm mapping
 */
var algorithmMap = {
  HS256: 'sha256',
  HS384: 'sha384',
  HS512: 'sha512',
  RS256: 'RSA-SHA256'
};

/**
 * Map algorithm to hmac or sign type, to determine which crypto function to use
 */
var typeMap = {
  HS256: 'hmac',
  HS384: 'hmac',
  HS512: 'hmac',
  RS256: 'sign'
};


/**
 * expose object
 */
var jwt = module.exports;


/**
 * version
 */
jwt.version = '0.5.6';

/**
 * Decode jwt
 *
 * @param {Object} token
 * @param {String} key
 * @param {Boolean} [noVerify]
 * @param {String} [algorithm]
 * @return {Object} payload
 * @api public
 */
jwt.decode = function jwt_decode(token, key, noVerify, algorithm) {
  // check token
  if (!token) {
    throw new Error('No token supplied');
  }
  // check segments
  var segments = token.split('.');
  if (segments.length !== 3) {
    throw new Error('Not enough or too many segments');
  }

  // All segment should be base64
  var headerSeg = segments[0];
  var payloadSeg = segments[1];
  var signatureSeg = segments[2];

  // base64 decode and parse JSON
  var header = JSON.parse(base64urlDecode(headerSeg));
  var payload = JSON.parse(base64urlDecode(payloadSeg));

  if (!noVerify) {
    if (!algorithm && /BEGIN( RSA)? PUBLIC KEY/.test(key.toString())) {
      algorithm = 'RS256';
    }

    var signingMethod = algorithmMap[algorithm || header.alg];
    var signingType = typeMap[algorithm || header.alg];
    if (!signingMethod || !signingType) {
      throw new Error('Algorithm not supported');
    }

    // verify signature. `sign` will return base64 string.
    var signingInput = [headerSeg, payloadSeg].join('.');
    if (!verify(signingInput, key, signingMethod, signingType, signatureSeg)) {
      throw new Error('Signature verification failed');
    }

    // Support for nbf and exp claims.
    // According to the RFC, they should be in seconds.
    if (payload.nbf && Date.now() < payload.nbf*1000) {
      throw new Error('Token not yet active');
    }

    if (payload.exp && Date.now() > payload.exp*1000) {
      throw new Error('Token expired');
    }
  }

  return payload;
};


/**
 * Encode jwt
 *
 * @param {Object} payload
 * @param {String} key
 * @param {String} algorithm
 * @param {Object} options
 * @return {String} token
 * @api public
 */
jwt.encode = function jwt_encode(payload, key, algorithm, options) {
  // Check key
  if (!key) {
    throw new Error('Require key');
  }

  // Check algorithm, default is HS256
  if (!algorithm) {
    algorithm = 'HS256';
  }

  var signingMethod = algorithmMap[algorithm];
  var signingType = typeMap[algorithm];
  if (!signingMethod || !signingType) {
    throw new Error('Algorithm not supported');
  }

  // header, typ is fixed value.
  var header = { typ: 'JWT', alg: algorithm };
  if (options && options.header) {
    assignProperties(header, options.header);
  }

  // create segments, all segments should be base64 string
  var segments = [];
  segments.push(base64urlEncode(JSON.stringify(header)));
  segments.push(base64urlEncode(JSON.stringify(payload)));
  segments.push(sign(segments.join('.'), key, signingMethod, signingType));

  return segments.join('.');
};

/**
 * private util functions
 */

function assignProperties(dest, source) {
  for (var attr in source) {
    if (source.hasOwnProperty(attr)) {
      dest[attr] = source[attr];
    }
  }
}

function verify(input, key, method, type, signature) {
  if(type === "hmac") {
    return (signature === sign(input, key, method, type));
  }
  else if(type == "sign") {
    return crypto.createVerify(method)
                 .update(input)
                 .verify(key, base64urlUnescape(signature), 'base64');
  }
  else {
    throw new Error('Algorithm type not recognized');
  }
}

function sign(input, key, method, type) {
  var base64str;
  if(type === "hmac") {
    base64str = crypto.createHmac(method, key).update(input).digest('base64');
  }
  else if(type == "sign") {
    base64str = crypto.createSign(method).update(input).sign(key, 'base64');
  }
  else {
    throw new Error('Algorithm type not recognized');
  }

  return base64urlEscape(base64str);
}

function base64urlDecode(str) {
  return Buffer.from(base64urlUnescape(str), 'base64').toString();
}

function base64urlUnescape(str) {
  str += new Array(5 - str.length % 4).join('=');
  return str.replace(/\-/g, '+').replace(/_/g, '/');
}

function base64urlEncode(str) {
  return base64urlEscape(Buffer.from(str).toString('base64'));
}

function base64urlEscape(str) {
  return str.replace(/\+/g, '-').replace(/\//g, '_').replace(/=/g, '');
}
});

var jwtSimple = jwt_1;

const db = uniCloud.database();
async function validateToken(token) {
	const userFromToken = JSON.parse(Buffer.from(token.split('.')[1], 'base64').toString());
	const userInDB = await db.collection('usermp').where(userFromToken).get();
	if (userInDB.data.length !== 1) {
		return {
			code: -1,
			errCode: 'TOKEN_INVALID',
			msg: '查无此人'
		}
	}
	const userInfoDB = userInDB.data[0];
	let userInfoDecode;

	userInfoDecode = jwtSimple.decode(token, userInfoDB.tokenSecret);

	function checkUser(userFromToken, userInfoDB) {
		return Object.keys(userFromToken).every(function(item) {
			return userFromToken[item] === userInfoDB[item] && userFromToken[item] === userInfoDecode[item]
		})
	}


	if (userInfoDB.exp > Date.now() && checkUser(userFromToken, userInfoDB)) {
		return {
			code: 0,
			username: userInfoDB.username,
			user_id:userInfoDB._id,
			msg: 'token验证成功'
		}
	}

	if (userInfoDB.exp < Date.now()) {
		return {
			code: -3,
			errCode: 'TOKEN_EXPIRED',
			msg: 'token已失效'
		}
	}

	return {
		code: -2,
		errCode: 'TOKEN_INVALID',
		msg: 'token无效'
	}

}

var validateToken_1 = {
	validateToken
};

/**
 * @description 更新订单状态(入参只能传1，2，3，4.其他状态无效)
 * @author unhejing (成都-敬宇杰)
 * @date 2020-02-26 下午5:37:46
 */
const {
  validateToken: validateToken$1
} = validateToken_1;
const db$1 = uniCloud.database();
const statusEnum = ['待付款','已取消','货到付款','已支付','已完成'];
var main = async (event, context) => {
	//event为客户端上传的参数
	console.log('event:' + event);
	//event.token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcGVuaWQiOiJvbHVYcjRsMFVrRnlTN3BoZ2JrUE92akR2UWJnIn0.gmAEd_I2AktSoElvsfWzpCVKlXlxE4wChweOD2Jqt4k"
	let tokenRes = await validateToken$1(event.token);
	if(tokenRes.code != 0){
		return tokenRes;
	}
	event.user_id = tokenRes.user_id;
	// event.user_id = "1";
	// event.status = 2;//订单状态
	if (event.user_id == '' || !event.user_id) {
		return {
			success: false,
			code: -1,
			msg: '参数错误，缺少用户id'
		}
	}
	if (event.id == '' || !event.id) {
		return {
			success: false,
			code: -1,
			msg: '参数错误，缺少订单id'
		}
	}
	if (event.status == '' || !event.status) {
		return {
			success: false,
			code: -1,
			msg: '参数错误，缺少订单状态status'
		}
	}
	event.status = parseInt(event.status);
	let effectiveStatus = [1,2,3,4];
	if (!effectiveStatus.includes(event.status)) {
		return {
			success: false,
			code: -1,
			msg: '订单状态无效'
		}
	}
	const collection = db$1.collection('order'); // 获取表'order'的集合对象
	let res = await collection.doc(event.id).get();
	console.log("订单响应数据:" + JSON.stringify(res));
	
	if (!res.data || res.data.length < 1) {
		return {
			success: false,
			code: -1,
			msg: '获取订单失败'
		}
	}
	let orderInfo = res.data[0];
	let currentStatus = orderInfo.status;
	if(event.status == 1 && currentStatus != 0){
		return {
			success: false,
			code: -1,
			msg: '当前订单状态为：'+statusEnum[currentStatus]+',无法取消'
		}
	}
	if(event.status == 2  && currentStatus != 0){
		return {
			success: false,
			code: -1,
			msg: '当前订单状态为：'+statusEnum[currentStatus]+',无法货到付款'
		}
	}
	if(event.status == 3  && currentStatus != 0){
		return {
			success: false,
			code: -1,
			msg: '当前订单状态为：'+statusEnum[currentStatus]+',无法变更为已支付'
		}
	}
	if(event.status == 4  && currentStatus != 2 && currentStatus != 3){
		return {
			success: false,
			code: -1,
			msg: '当前订单状态为：'+statusEnum[currentStatus]+',无法变更为已完成'
		}
	}
	
	let orderData = await collection.doc(event.id).update({
		status: event.status
	});
	console.log("更改订单状态响应内容："+JSON.stringify(orderData));
	if(orderData.affectedDocs > 0){
		return {
			success: true,
			code: 0,
			msg: '更新状态成功'
		}
	}
	return {
		success: false,
		code: -1,
		msg: '服务器内部错误'
	}

};

var updateOrderStatus = {
	main: main
};

exports.default = updateOrderStatus;
exports.main = main;
