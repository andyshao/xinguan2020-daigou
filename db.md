数据库设计

# admin
```json
{
    "_id": "", // string，自生成
    "guid": "", // string 用户唯一标识，注册时生成无法修改
    "username": "", // string 用户
    "password": "", // string 密码(禁止明文)
    "status": 0, // int 0正常 1冻结
    "permission": 0, // int 默认2，0超级管理员, 1普通管理员, 2普通
    "create_time": 0, // 时间戳 GMT
    "create_ip": "" // 注册 ip
}
```


# user（已变更，如下）
```json
{
    "_id": "", // string，自生成
    "guid": "", // string 用户唯一标识，注册时生成无法修改
    "wx_open_id": "", // string 关联微信openid  用户code 换取
    "name": "", // string 自动从微信获取
    "phone": "", // string 不能为空，需验证符合规则
	"address":"", // string 不能为空
    "photo": "", // string 自动从微信获取
    "create_time": 0, // 时间戳 GMT
    "create_ip": "" // 注册 ip
}
```
# user 目前的用户表
```json
{
	"_id": "",// String用户唯一标识，注册时生成无法修改
	"exp": 1585154886651,// 时间戳，过期时间
	"openid": "oluXr4l0UkFyS7phgbkPOvjDvQbg",// String 用户的微信登录的openid
	"tokenSecret": "d184e2fc3a811f72e6ef32637a168431" //登录时生成的token密钥
}
```

# user_info 用户基本信息表
```json
{
	"id":"",// String用户唯一标识，注册时生成无法修改
	"name":"",// 联系人姓名
	"address":"",// 收货地址
	"user_id":"",// 关联用户表的用户id
	"phone":"",// 联系人电话
	"create_time": 0,// 时间戳，创建时间
	"update_time": 0 // 时间戳，更新时间
}
```

# product // 商品表
```json
{
    "_id":"", // string，自生成
    "product_code":"", //string 商品编码
    "product_name":"", //string 商品名称
    "sale_price":0,//double 商品销售价格
    "book_price":0,//double 商品代订价格
    "sale_unit":"",//string 销售单位
    "introduction":"",//string html格式展示， 产品描述
    "primary_img":"",//string 商品主图
    "publish_status":0,//上下架状态,0 下架 1 上架
    "create_time":0, // 时间戳 GMT, 创建时间
    "update_time":0, // 时间戳 GMT, 更新时间
    "create_by":"",//string 创建人
    "update_by":""//string 更新人
}
```
# product_img // 商品图片表
```json
{
    "_id":"", // string，自生成
    "img_path":"",//string 商品主图
    "is_primary":0, //int 是否是主图 0 否 1 是
    "product_id":"" // string 商品id
}
```
# order // 订单表
```json
{
    "_id":"", // string，自生成
    "order_code":"",// string 订单编码
    "user_id":"",//string 用户id
    "link_phone":"",// string 联系手机号
    "address":"",//string 收货地址
    "consignee_name":"",//string 收货人姓名
    "amount_payable":0,//double 应付金额
    "real_pay":0,//double 订单实付金额
	"pay_type":0,// int 支付类型 0 现金支付 1 微信支付
	"status":0,// int订单状态，0 待付款 1 已取消 2 到付 3 已支付 4 已完成 （查询status全部传null或者不传，待付款传0（状态为0），待收货传1（状态为2，3））
    "remark":"",//string 备注
    "create_time":0, // 时间戳 GMT, 创建时间
    "update_time":0 // 时间戳 GMT, 更新时间
}
```
# order_product // 订单商品表
```json
{
    "_id":"", // string，自生成
    "order_code":"",// string 订单编码
    "product_id":"",// string 商品id（冗余，方便查询商品）
    "product_code":"",// string 商品编码
    "product_name":"",// string 商品名称
    "primary_img":"",// string 商品主图
    "book_price":0.00,//double 商品价格
    "num":0,// int 商品数量
    "sum_price":0.00,// double 商品合计价格
    "sale_unit":"",//string 销售单位
    "create_time":0 // 时间戳 GMT, 创建时间
}
```
# pay_detail // 支付明细表
```json
{
    "_id":"", // string，自生成
    "order_code":"",// string 订单编码（正向或逆向）
    "user_id":"",//用户id
    "username":"",// 用户名
    "phone":"",// 联系电话
    "serial_number":"",// 交易流水号
    "pay_price":0.00,// double 支付价格
    "payment_type":0,// int 支付类型 0 支付 1 退款
    "create_time":0 // 时间戳 GMT, 创建时间
}
```
# cart // 购物车
```json
{
    "_id":"", // string，自生成
    "user_id":"",//用户id
	"product_id":"",// 商品id
	"num":0// int 商品数量
}
```